class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.926608,-74.173430];
        this.escalaInicial=17;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial, this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL, this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);

    }

    colocarMarcador(posicion){
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){
        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }


    colocarPoligono(posicion, configuracion){

        this.poligonos.push(L.polygon(posicion,configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}
let miMapa=new Mapa();

//Marcador y círculo de mi barrio
miMapa.colocarMarcador([4.926608,-74.173430]);
miMapa.colocarCirculo([4.926608,-74.173430],{
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});

miMapa.colocarMarcador([4.926608,-74.173430]);

//Marcador parque central.
miMapa.colocarCirculo([4.928029,-74.173821], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 50
});

miMapa.colocarMarcador([4.928029,-74.173821]);

//Poligono iglesia municipal
miMapa.colocarPoligono([[4.928345,-74.173418],[4.928596,-74.173075],[4.928729,-74.173203],[4.928478,-74.173542]],{

    color:'yellow',
    fillColor:'yellow',
    fillOpacity: 0.5,


}); 

//Poligono escuela  departamental 
miMapa.colocarPoligono([[4.928521,-74.172427],[4.928093,-74.172083],[4.928617,-74.171568],[4.929077,-74.171901]],{

    color:'yellow',
    fillColor:'yellow',
    fillOpacity: 0.5,


}); 
